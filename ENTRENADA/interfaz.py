from Tkinter import *
import Tkinter, Tkconstants, tkFileDialog
import Tkinter
import tkMessageBox
from PIL import Image, ImageOps
import validar
import numpy as np
class GUI(Frame):
	ruta='/'
	im = None
	ann = None
	def __init__(self,master=None):
		Frame.__init__(self)
		self.ann = validar.cargarRed()
		def btnaceptarClick():
			root.filename = tkFileDialog.askopenfilename(initialdir = "/home/desktop",
				title = "Buscar imagen",filetypes = (("jpeg files","*.jpg | *.jpeg"),("all files","*.*")))
			self.ruta=root.filename
			if(self.im != None):
				self.im.close()
			self.im = Image.open(self.ruta)
			self.im.show()
		def hola():
			if(self.ruta!=None):
				origin = validar.validar(self.ruta,self.ann)
				ron= [ round(num,0) for num in origin]
				print ron
				if np.sum(ron)>1 or np.sum(ron)==0 :
					tkMessageBox.showinfo('ERROR',"No se reconoce")
				else:
					if ron[0]==1:
						tkMessageBox.showinfo('Fresa',"Fresa Madura")
					elif ron[1]==1:
						tkMessageBox.showinfo('Fresa',"Fresa Pasada")
					else:
						tkMessageBox.showinfo('Fresa',"Fresa Podrida")
		def imagenAEpisodio(imagen):
			image = Image.open(imagen)
			pixeles = list(image.getdata())
			lista=[]
			for x in pixeles:
				lista.append(x[0])
				lista.append(x[1])
				lista.append(x[2])
			return lista
		self.btnaceptar = Button(root, command=btnaceptarClick, text="BUSCAR")
		self.btnaceptar.pack(fill=X,pady=10,padx=100)
		self.boton = Button(root,command=hola,text="VALIDAR")
		self.boton.pack(fill=X,pady=10,padx=100)
root = Tk()
root.title("Visor")
root.resizable(width=FALSE, height=FALSE)
guiframe=GUI(root)
guiframe.pack()
root.mainloop()
