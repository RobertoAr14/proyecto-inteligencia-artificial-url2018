from fann2 import libfann as fann
import juanIm as ji
import cv2
def imagenAEpisodio(imagen):
	lista=[]
	for fil in imagen:
		for col in fil:
			for x in col:
				lista.append(x)
	return lista
def cargarRed(ruta="red20.net"):
	ann = fann.neural_net()
	ann.create_from_file(ruta)
	return ann
def validar(ruta,ann): 
	im=cv2.resize(ji.procesarFoto(ruta)[0],(50,50))
	#cv2.imshow(ruta,im)
	#cv2.waitKey(0)
	entrada = imagenAEpisodio(im)
	entrada
#print len(entrada)
	return ann.run(entrada)