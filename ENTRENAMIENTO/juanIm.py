import numpy as np
import cv2
def procesarFoto(ruta):
	lista=[]
	####################FILTROS################
	src = cv2.imread(ruta)
	gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (51, 51), 3) #51,51,3
	t, dst = cv2.threshold(gray, 200, 700, cv2.THRESH_BINARY)
	canny = cv2.Canny(dst, 10 , 100)#10 100
	#cv2.imshow('CANNY', canny)
	#cv2.imshow('umbral', gray)
	#cv2.imshow('result', dst)

	###################CONTORNOS#############
	contornos = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
	#cv2.drawContours(canny, contornos, -1, (128,0,0), 2)
	#cv2.imshow("contornos",canny)

	###################RECTANGULO############
	for c in contornos:
	    area = cv2.contourArea(c)
	    if area > 2000: #and area < 118000:  
	        x, y, w, h = cv2.boundingRect(c)
	        cv2.rectangle(canny, (x, y), (x + w, y + h), (200, 200, 0), 1) 
	        crop_img = src[y:y+h, x:x+w]
	        lista.append(crop_img)
	        #cv2.imshow("a",crop_img)
	return lista
#procesarFoto('c1.jpeg')
#cv2.waitKey(0)
        #cv2.imshow("Recorte"+str(c), crop_img)
#cv2.imshow("Rectangulos", canny)
#cv2.imshow("Recorte", crop_img)